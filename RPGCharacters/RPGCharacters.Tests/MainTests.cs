using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGCharacters.Models;
using RPGCharacters.Enums;
using RPGCharacters.Exceptions;

namespace RPGCharacters.Tests
{
    [TestClass]
    public class MainTests
    {
        Character character = new Character(ClassType.WARRIOR, "Casper");

        Weapon testAxe = new Weapon(
            name: "Common axe",
            requiredLevel: 1,
            slot: SlotType.WEAPON,
            type: WeaponType.AXE,
            new WeaponAttributes(7, 1.1)
        );

        Armor testPlateBody = new Armor(
            name: "Common plate body armor",
            requiredLevel: 1,
            slot: SlotType.BODY,
            type: ArmorType.PLATE,
            new Attributes { Strength = 1 }
        );

        Weapon testBow = new Weapon(
            name: "Common bow",
            requiredLevel: 1,
            slot: SlotType.WEAPON,
            type: WeaponType.BOW,
            new WeaponAttributes(12, 0.8)
        );

        Armor testClothHead = new Armor(
            name: "Common cloth head armor",
            requiredLevel: 1,
            slot: SlotType.HEAD,
            type: ArmorType.CLOTH,
            new Attributes { Intelligence = 5 }
        );

        [TestMethod]
        public void WeaponEquipping_AWeaponWithTooHighLevel_ShouldThrowError()
        {
            // Arrange 
            character.Level = 1;
            testAxe.RequiredLevel = 2;

            // Act & Assert
            Assert.ThrowsException<InvalidWeaponException>(() => character.EquipWeapon(testAxe));
        }

        [TestMethod]
        public void ArmorEquipping_APieceOfArmorWithTooHighLevel_ShouldThrowError()
        {
            // Arrange 
            character.Level = 1;
            testPlateBody.RequiredLevel = 2;

            // Act & Assert
            Assert.ThrowsException<InvalidArmorException>(() => character.EquipArmor(testPlateBody));
        }

        [TestMethod]
        public void WeaponEquipping_AWeaponWithWrongType_ShouldThrowError()
        {
            // Act & Assert
            Assert.ThrowsException<InvalidWeaponException>(() => character.EquipWeapon(testBow));
        }

        [TestMethod]
        public void ArmorEquipping_APieceOfArmorWithWrongType_ShouldThrowError()
        {
            // Act & Assert
            Assert.ThrowsException<InvalidArmorException>(() => character.EquipArmor(testClothHead));
        }

        [TestMethod]
        public void WeaponEquipping_AWeapon_ShouldReturnSuccessMessage()
        {
            // Arrange & Act
            string message = character.EquipWeapon(testAxe);

            // Assert
            Assert.AreEqual(message, "New weapon equipped!");
        }

        [TestMethod]
        public void ArmorEquipping_APieceOfArmor_ShouldReturnSuccessMessage()
        {
            // Arrange & Act
            string message = character.EquipArmor(testPlateBody);

            // Assert
            Assert.AreEqual(message, "New armor equipped!");
        }

        [TestMethod]
        public void DealingDamage_WithDefaultWarrior_ShouldHaveCorrectDamageValue()
        {
            // Arrange
            Character character = new Character(ClassType.WARRIOR, "Casper");
            double expectedDamage = 1.0 + (5.0 / 100.0);

            // Act
            double damage = character.GetExpectedDamage();

            // Assert
            Assert.AreEqual(damage, expectedDamage);
        }

        [TestMethod]
        public void DealingDamage_WithDefaultWarriorAndWeapon_ShouldHaveCorrectDamageValue()
        {
            // Arrange
            Character character = new Character(ClassType.WARRIOR, "Casper");
            double expectedDamage = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));

            // Act
            character.EquipWeapon(testAxe);
            double damage = character.GetExpectedDamage();

            // Assert
            Assert.AreEqual(damage, expectedDamage);
        }

        [TestMethod]
        public void DealingDamage_WithDefaultWarriorAndWeaponAndArmor_ShouldHaveCorrectDamageValue()
        {
            // Arrange
            Character character = new Character(ClassType.WARRIOR, "Casper");
            double expectedDamage = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0));

            // Act
            character.EquipWeapon(testAxe);
            character.EquipArmor(testPlateBody);
            double damage = character.GetExpectedDamage();

            // Assert
            Assert.AreEqual(damage, expectedDamage);
        }
    }
}