using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGCharacters.Models;
using RPGCharacters.Enums;

namespace RPGCharacters.Tests
{
    [TestClass]
    public class LevelUpTests
    {

        [TestMethod]
        public void CharacterConstructor_ByDefault_IsLevelOne()
        {
            // Arrange
            ClassType type = ClassType.WARRIOR;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);

            // Assert
            Assert.AreEqual(character.Level, 1);
        }
        [TestMethod]
        public void LevelUp_IsUsedOnce_ShouldBeOneLevelHigher()
        {
            // Arrange
            ClassType type = ClassType.WARRIOR;
            string name = "Simon";
            Character character = new Character(type, name);

            // Act 
            character.LevelUp();

            // Assert
            Assert.AreEqual(character.Level, 2);
        }

        [TestMethod]
        public void LevelUp_ForMage_ShouldHaveCorrectNewStats()
        {
            // Arrange
            ClassType type = ClassType.MAGE;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);
            character.LevelUp();

            // Assert
            Attributes leveledUpMageAttributes = new Attributes { Strength = 2, Dexterity = 2, Intelligence = 13 };
            Assert.IsTrue(character.Attributes.Equals(leveledUpMageAttributes));
        }
        [TestMethod]
        public void LevelUp_ForWarrior_ShouldHaveCorrectNewStats()
        {
            // Arrange
            ClassType type = ClassType.WARRIOR;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);
            character.LevelUp();

            // Assert
            Attributes leveledUpWarriorAttributes = new Attributes { Strength = 8, Dexterity = 4, Intelligence = 2 };
            Assert.IsTrue(character.Attributes.Equals(leveledUpWarriorAttributes));
        }
        [TestMethod]
        public void LevelUp_ForRogue_ShouldHaveCorrectNewStats()
        {
            // Arrange
            ClassType type = ClassType.ROGUE;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);
            character.LevelUp();

            // Assert
            Attributes leveledUpRogueAttributes = new Attributes { Strength = 3, Dexterity = 10, Intelligence = 2 };
            Assert.IsTrue(character.Attributes.Equals(leveledUpRogueAttributes));
        }
        [TestMethod]
        public void LevelUp_ForRanger_ShouldHaveCorrectNewStats()
        {
            // Arrange
            ClassType type = ClassType.RANGER;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);
            character.LevelUp();

            // Assert
            Attributes leveledUpRangerAttributes = new Attributes { Strength = 2, Dexterity = 12, Intelligence = 2 };
            Assert.IsTrue(character.Attributes.Equals(leveledUpRangerAttributes));
        }
    }
}