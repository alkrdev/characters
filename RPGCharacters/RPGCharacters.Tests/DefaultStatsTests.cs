using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGCharacters.Models;
using RPGCharacters.Enums;

namespace RPGCharacters.Tests
{
    [TestClass]
    public class DefaultStatsTests
    {

        [TestMethod]
        public void CharacterCreation_ForMage_ShouldHaveCorrectStats()
        {
            // Arrange
            ClassType type = ClassType.MAGE;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);

            // Assert
            Attributes defaultMageAttributes = new Attributes { Strength = 1, Dexterity = 1, Intelligence = 8 };
            Assert.IsTrue(character.Attributes.Equals(defaultMageAttributes));
        }
        [TestMethod]
        public void CharacterCreation_ForWarrior_ShouldHaveCorrectStats()
        {
            // Arrange
            ClassType type = ClassType.WARRIOR;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);

            // Assert
            Attributes defaultWarriorAttributes = new Attributes { Strength = 5, Dexterity = 2, Intelligence = 1 };
            Assert.IsTrue(character.Attributes.Equals(defaultWarriorAttributes));
        }
        [TestMethod]
        public void CharacterCreation_ForRogue_ShouldHaveCorrectStats()
        {
            // Arrange
            ClassType type = ClassType.ROGUE;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);

            // Assert
            Attributes defaultRogueAttributes = new Attributes { Strength = 2, Dexterity = 6, Intelligence = 1 };
            Assert.IsTrue(character.Attributes.Equals(defaultRogueAttributes));
        }
        [TestMethod]
        public void CharacterCreation_ForRanger_ShouldHaveCorrectStats()
        {
            // Arrange
            ClassType type = ClassType.RANGER;
            string name = "Simon";

            // Act 
            Character character = new Character(type, name);

            // Assert
            Attributes defaultRangerAttributes = new Attributes { Strength = 1, Dexterity = 7, Intelligence = 1 };
            Assert.IsTrue(character.Attributes.Equals(defaultRangerAttributes));
        }
    }
}