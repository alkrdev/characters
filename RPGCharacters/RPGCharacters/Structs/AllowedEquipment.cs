﻿using RPGCharacters.Enums;

namespace RPGCharacters.Structs
{
    public struct AllowedEquipment
    {
        public List<WeaponType> Weapons;
        public List<ArmorType> Armors;
    }
}
