﻿namespace RPGCharacters.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string message)
        {
            Console.WriteLine(message);
        }
    }
}
