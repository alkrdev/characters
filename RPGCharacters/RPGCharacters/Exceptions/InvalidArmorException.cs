﻿namespace RPGCharacters.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string message)
        {
            Console.WriteLine(message);
        }
    }
}
