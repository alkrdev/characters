﻿namespace RPGCharacters.Enums
{
    public enum ClassType
    {
        NONE = -1,
        MAGE,
        RANGER,
        ROGUE,
        WARRIOR,
    }
}
