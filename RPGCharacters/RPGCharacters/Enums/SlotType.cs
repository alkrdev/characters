﻿namespace RPGCharacters.Enums
{
    public enum SlotType
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
