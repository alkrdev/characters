﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models
{
    public class Attributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }


        public Attributes(ClassType type)
        {
            switch (type)
            {
                case ClassType.MAGE:
                    Strength = 1;
                    Dexterity = 1;
                    Intelligence = 8;
                    break;
                case ClassType.RANGER:
                    Strength = 1;
                    Dexterity = 7;
                    Intelligence = 1;
                    break;
                case ClassType.ROGUE:
                    Strength = 2;
                    Dexterity = 6;
                    Intelligence = 1;
                    break;
                case ClassType.WARRIOR:
                    Strength = 5;
                    Dexterity = 2;
                    Intelligence = 1;
                    break;
                default:
                    break;
            }
        }

        public Attributes()
        {

        }
        
        /// <summary>
         /// Increases the 3 attributes by the amount provided for each of the parameters
         /// </summary>
         /// <param name="str"></param>
         /// <param name="dex"></param>
         /// <param name="intel"></param>
        public void IncreaseAttributes(int str, int dex, int intel)
        {
            Strength += str;
            Dexterity += dex;
            Intelligence += intel;
        }

        /// <summary>
        /// Method used to compare 2 instances of the same class for equality
        /// </summary>
        /// <param name="other"></param>
        /// <returns>true if equal; otherwise it returns false</returns>
        public bool Equals(Attributes other)
        {
            if (Strength != other.Strength) return false;
            if (Dexterity != other.Dexterity) return false;
            if (Intelligence != other.Intelligence) return false;

            return true;
        }
    }
}
