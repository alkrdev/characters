﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models
{
    public class Armor : Item
    {
        public ArmorType Type { get; set; }
        public SlotType Slot { get; set; }
        public Attributes Attributes { get; set; }

        public Armor(string name, int requiredLevel, SlotType slot, ArmorType type, Attributes attributes) : base(name, requiredLevel)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
            Type = type;
            Attributes = attributes;
        }
    }
}
