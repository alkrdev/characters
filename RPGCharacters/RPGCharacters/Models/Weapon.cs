﻿using RPGCharacters.Enums;

namespace RPGCharacters.Models
{
    public class Weapon : Item
    {
        public WeaponType Type { get; set; }
        public WeaponAttributes Attributes { get; set; }
        public SlotType Slot { get; set; }
        
        /// <summary>
        /// Calculates the "Damage Per Second" based on the attack speed and damage of the weapon
        /// </summary>
        /// <returns>The "DPS" of the weapon as a double</returns>
        public double GetDPS()
        {
            return Attributes.Damage * Attributes.AttackSpeed;
        }

        public Weapon(string name, int requiredLevel, SlotType slot, WeaponType type, WeaponAttributes weaponAttributes) : base(name, requiredLevel)
        {
            Slot = slot;
            Type = type;
            Attributes = weaponAttributes;
        }
    }
}
