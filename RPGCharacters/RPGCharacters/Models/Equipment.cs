﻿using RPGCharacters.Enums;

namespace RPGCharacters.Models
{
    public class Equipment
    {
        public Weapon Weapon { get; set; }
        public Dictionary<SlotType, Armor> Worn { get; set; }

        public Equipment()
        {
            Worn = new Dictionary<SlotType, Armor>();
        }
    }
}
