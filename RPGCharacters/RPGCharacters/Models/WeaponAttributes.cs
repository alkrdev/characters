﻿namespace RPGCharacters.Models
{
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
        public WeaponAttributes(int damage, double attackSpeed)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
        }
    }
}
