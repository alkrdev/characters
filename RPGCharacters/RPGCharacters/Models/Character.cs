using RPGCharacters.Enums;
using RPGCharacters.Exceptions;
using RPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models
{
    public class Character
    {
        public event Action<ClassType> CharacterLeveledUp;

        private AllowedEquipment AllowedEquipment;
        public ClassType Type { get; }
        public string Name { get; }
        public Attributes Attributes { get; }
        public Equipment Equipment { get; } = new Equipment();
        public int Level { get; set; } = 1;

        public Character(ClassType type, string name)
        {
            Type = type;
            Name = name;
            Attributes = new Attributes(Type);
            CharacterLeveledUp += new Action<ClassType>(HandleLevelUpEvent);
            AllowedEquipment.Weapons = GetAllowedWeaponTypes(Type);
            AllowedEquipment.Armors = GetAllowedArmorTypes(Type);
        }

        /// <summary>
        /// Increases the level of the character, and fires the CharacterLeveledUp Event
        /// </summary>
        public void LevelUp ()
        {
            Level++;
            CharacterLeveledUp(Type);
        }

        /// <summary>
        /// Used as an eventhandler, increases stats of character when the levelup event happens
        /// </summary>
        /// <param name="type"></param>
        private void HandleLevelUpEvent(ClassType type)
        {
            switch (type)
            {
                case ClassType.MAGE:
                    Attributes.IncreaseAttributes(1, 1, 5);
                    break;
                case ClassType.RANGER:
                    Attributes.IncreaseAttributes(1, 5, 1);
                    break;
                case ClassType.ROGUE:
                    Attributes.IncreaseAttributes(1, 4, 1);
                    break;
                case ClassType.WARRIOR:
                    Attributes.IncreaseAttributes(3, 2, 1);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// "Equips" the provided weapon by setting the current weapon to it. 
        /// Throws InvalidWeaponException if the level of the weapon is too high, 
        /// or the character isn't allowed to equip the type of the weapon
        /// </summary>
        /// <param name="weap"></param>
        /// <returns>"Success" Message as string</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public string EquipWeapon(Weapon weap)
        {
            if (weap.RequiredLevel > Level) throw new InvalidWeaponException("The characters level is too low");
            if (!AllowedEquipment.Weapons.Contains(weap.Type)) throw new InvalidWeaponException($"A {Type} cannot equip a {weap.Type}");

            Equipment.Weapon = weap;
            return "New weapon equipped!";
        }

        /// <summary>
        /// "Equips" the provided armor by setting the current armor to it. 
        /// Throws InvalidArmorException if the level of the armor is too high, 
        /// or the character isn't allowed to equip the type of the armor
        /// </summary>
        /// <param name="arm"></param>
        /// <returns>"Success" Message as string</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public string EquipArmor(Armor arm)
        {
            if (arm.RequiredLevel > Level) throw new InvalidArmorException("The characters level is too low");
            if (!AllowedEquipment.Armors.Contains(arm.Type)) throw new InvalidArmorException($"A {Type} cannot equip a {arm.Type} type armor");
            if (Equipment.Worn.TryGetValue(arm.Slot, out _)) 
            {
                Equipment.Worn[arm.Slot] = arm;
            } 
            else
            {
                Equipment.Worn.Add(arm.Slot, arm);
            }
            return "New armor equipped!";
        }

        /// <summary>
        /// Gets the expected damage dealt, based on a specific formula that 
        /// includes the calculated "DPS" of the weapon and the primary attribute of the character
        /// </summary>
        /// <returns>The expected damage as a double</returns>
        public double GetExpectedDamage()
        {
            double WeaponDPS = Equipment.Weapon != null ? Equipment.Weapon.GetDPS() : 1;

            decimal decimalCalculation = (decimal)GetTotalPrimaryAttribute() / 100m;

            return WeaponDPS * (1 + (double)decimalCalculation);
        }

        /// <summary>
        /// Gets the full amount of the primary stat, based on equipment and base stats
        /// </summary>
        /// <returns>Total from Attributes added to Total from equipment as a double</returns>
        public double GetTotalPrimaryAttribute()
        {
            string stat = Type switch
            {
                ClassType.MAGE => "Intelligence",
                ClassType.RANGER => "Dexterity",
                ClassType.ROGUE => "Dexterity",
                ClassType.WARRIOR => "Strength",
                _ => ""
            };

            var attributesType = typeof(Attributes);
            var property = attributesType.GetProperty(stat);

            double TotalFromAttributes = (int)property.GetValue(Attributes);

            double TotalFromEquipment = Equipment.Worn is not null 
                ? Equipment.Worn.Sum(x => (int)property.GetValue(x.Value.Attributes)) 
                : 0;

            return TotalFromAttributes + TotalFromEquipment;
        }

        /// <summary>
        /// Gets the attribute increases from the all the worn equipment
        /// </summary>
        /// <returns>A tuple with the 3 different attributes as doubles</returns>
        public (double strength, double dexterity, double intelligence) GetFullStats()
        {
            var values = Equipment.Worn.Values;

            double allStrength = values.Sum(x => x.Attributes.Strength) + Attributes.Strength;
            double allDexterity = values.Sum(x => x.Attributes.Dexterity) + Attributes.Dexterity;
            double allIntelligence = values.Sum(x => x.Attributes.Intelligence) + Attributes.Intelligence;

            return (allStrength, allDexterity, allIntelligence);
        }

        /// <summary>
        /// Based on the given ClassType, returns the allowed weapon types for that ClassType
        /// </summary>
        /// <param name="type"></param>
        /// <returns>List of WeaponTypes</returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<WeaponType> GetAllowedWeaponTypes(ClassType type) => type switch
        {
            ClassType.MAGE => new List<WeaponType>() { WeaponType.STAFF, WeaponType.WAND },
            ClassType.RANGER => new List<WeaponType>() { WeaponType.BOW },
            ClassType.WARRIOR => new List<WeaponType>() { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD },
            ClassType.ROGUE => new List<WeaponType>() { WeaponType.DAGGER, WeaponType.SWORD },
            _ => throw new NotImplementedException()
        };

        /// <summary>
        /// Based on the given ClassType, returns the allowed armor types for that ClassType
        /// </summary>
        /// <param name="type"></param>
        /// <returns>List of ArmorTypes</returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<ArmorType> GetAllowedArmorTypes(ClassType type) => type switch
        {
            ClassType.MAGE => new List<ArmorType>() { ArmorType.CLOTH },
            ClassType.RANGER => new List<ArmorType>() { ArmorType.LEATHER, ArmorType.MAIL },
            ClassType.WARRIOR => new List<ArmorType>() { ArmorType.MAIL, ArmorType.PLATE },
            ClassType.ROGUE => new List<ArmorType>() { ArmorType.LEATHER, ArmorType.MAIL },
            _ => throw new NotImplementedException()
        };


        /// <summary>
        /// Artificially provides the given character, with "gear"
        /// </summary>
        /// <param name="character"></param>
        public void GearUpCharacter()
        {

            Weapon commonWeapon = new Weapon(
                name: "Common weapon",
                requiredLevel: 1,
                slot: SlotType.WEAPON,

                type: Type == ClassType.MAGE ? WeaponType.STAFF :
                    Type == ClassType.RANGER ? WeaponType.BOW :
                    WeaponType.SWORD,

                new WeaponAttributes(7, 1.1)
            );

            var armortype = Type == ClassType.MAGE ? ArmorType.CLOTH : ArmorType.MAIL;

            Armor commonBody = new Armor(
                name: "Common armor",
                requiredLevel: 1,
                slot: SlotType.BODY,
                type: armortype,
                new Attributes { Strength = 1, Dexterity = 2, Intelligence = 3 }
            );

            Armor commonHead = new Armor(
                name: "Common armor",
                requiredLevel: 1,
                slot: SlotType.HEAD,
                type: armortype,
                new Attributes { Strength = 2, Dexterity = 3, Intelligence = 1 }
            );

            Armor commonLegs = new Armor(
                name: "Common armor",
                requiredLevel: 1,
                slot: SlotType.LEGS,
                type: armortype,
                new Attributes { Strength = 3, Dexterity = 1, Intelligence = 2 }
            );

            EquipWeapon(commonWeapon);
            EquipArmor(commonBody);
            EquipArmor(commonHead);
            EquipArmor(commonLegs);
        }
    }
}
