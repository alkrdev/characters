﻿using RPGCharacters.Models;
using RPGCharacters.Enums;
using RPGCharacters.Exceptions;
using System.Text;

namespace Surface
{
    public class Program
    {
        /// <summary>
        /// Main function that runs on execution
        /// </summary>
        public static void Main()
        {
            ClassType type;
            do
            {
                Console.WriteLine("CREATE A NEW CHARACTER BY PRESSING ONE OF THE FOLLOWING BUTTONS");
                Console.WriteLine("M TO CREATE A MAGE");
                Console.WriteLine("W TO CREATE A WARRIOR");
                Console.WriteLine("R TO CREATE A ROGUE");
                Console.WriteLine("A TO CREATE A RANGER");

                var input = Console.ReadKey(true);

                Console.Clear();

                type = input.Key switch
                {
                    ConsoleKey.M => ClassType.MAGE,
                    ConsoleKey.W => ClassType.WARRIOR,
                    ConsoleKey.R => ClassType.ROGUE,
                    ConsoleKey.A => ClassType.RANGER,
                    _ => ClassType.NONE
                };

            } while (type == ClassType.NONE);

            Character character = new Character(type, "Simon");
            bool isWearingGear = false;

            while(true)
            {
                Console.WriteLine("=====================================================" + Environment.NewLine);
                Console.WriteLine("Current Character:");
                PrintCharacterToConsole(character);
                Console.WriteLine("=====================================================");

                Console.WriteLine("USE THE FOLLOWING COMMANDS TO CHANGE YOUR CHARACTER: ");
                Console.WriteLine("1. Level Up!");
                Console.WriteLine("2. Increase Strength!");
                Console.WriteLine("3. Increase Dexterity!");
                Console.WriteLine("4. Increase Intelligence!");
                if (!isWearingGear)
                {
                    Console.WriteLine("5. Gear Up! (Once)");
                }



                var keyPressed = Console.ReadKey(true);

                switch (keyPressed.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        character.LevelUp();
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        character.Attributes.IncreaseAttributes(1, 0, 0);
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        character.Attributes.IncreaseAttributes(0, 1, 0);
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        character.Attributes.IncreaseAttributes(0, 0, 1);
                        break;
                    case ConsoleKey.D5 when (!isWearingGear):
                    case ConsoleKey.NumPad5 when (!isWearingGear):
                        character.GearUpCharacter();
                        isWearingGear = true;
                        break;
                    default:
                        break;
                }
                Console.Clear();
            }
        }        

        /// <summary>
        /// Prints the various details of the character to the screen by way of a StringBuilder
        /// </summary>
        /// <param name="character"></param>
        private static void PrintCharacterToConsole(Character character)
        {
            StringBuilder sb = new StringBuilder();

            (double strength, double dexterity, double intelligence) = character.GetFullStats();

            sb.AppendLine($"Character Name: { character.Name }");
            sb.AppendLine($"Character Class: { character.Type }");
            sb.AppendLine($"Character Level: { character.Level }");
            sb.AppendLine($"Character Strength: { strength }");
            sb.AppendLine($"Character Dexterity: { dexterity }");
            sb.AppendLine($"Character Intelligence: { intelligence }");
            sb.AppendLine($"Character Damage: { Math.Floor(character.GetExpectedDamage() * 100) / 100 }");


            Console.WriteLine(sb);
        }
    }
}


//Console.WriteLine("Level: " + character.Level);